# 신입 두 번째 응용 과제 - NAT 홈페이지

## NAT 홈페이지란?

NAT 홈페이지는 NAT 소프트웨어 회사의 홈페이지를 말한다.

주요 동적 기능으로는 GoogleMap을 이용한 약도 안내, 댓글을 달수 있는 토론 시스템이다.

Front-End : HTML5, CSS3(Bootstrap), JavaScript ,
Back-End : Play Framework, MySql, Ubuntu 를 응용하여 개발되었다.



## Getting Started

[NAT.zip 압축파일](https://bitbucket.org/toyou04net/------------NAT.zip)을 다운로드 하고
압축을 해제한다.

참고) NAT.zip 파일의 구성

- index.html : 메인화면(시작화면)
- notice.html : 공지사항
- greeting.html : 인사말
- loadmap.html : 찾아오는길
- automationBusiness.html : 자동화사업팀
- solutionBusiness.html : 솔루션사업팀
- business1~3.html : 메인화면 상단 사진의 상세내용
- join.html : 회원가입
- login.html : 로그인
- CSS 폴더 : CSS 파일
- fonts 폴더 : 글꼴 파일
- images 폴더 : 그림 파일
- js 폴더 : Jquery 파일

### NAT 홈페이지를 둘러보는 방법

1. GUI(Graphical User Interface)에서 index.html 파일을 IE10, Chrome, Fire Fox, Safari 브라우저를 이용하여 실행한다.


### 구동 화면
참고) 좌 : index.html 우 : -------.bat
![구동 화면](https://bitbucket.org/toyou04net--------.jpg)

--------------------
엔에이티

(NAT 기술지원) 배도영
toyou04net@nat21c.com 010-3201-1591

(NAT 디자인지원) 김미진
jjong0602@nat21c.com 010-5035-2234
